var BSTable = function (options, $this) {
    // 当前Grid对象元素，默认id：dataGrid
    var btGrid = typeof $this != 'undefined' ? $this : options.btGrid ? options.btGrid : $("#dataGrid");

    // 当前Grid的数据来源Form，默认id：searchForm
    var searchForm = options.searchForm ? options.searchForm : $("#searchForm");

    options = $.extend({
        url: searchForm.attr('action'),
        btnSearch: $("#btnSearch"), 					// 查询按钮
        height: ($(window).height()-52),

        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        method: "POST",
        ajaxOptions:{
            beforeSend: function (xhr) {
                js.loading();
            }},
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: options.treeEnable?false:true,                   //是否显示分页（*）
        paginationLoop: false,
        sortable: options.treeEnable?false:true,                     //是否启用排序
            silentSort: true,
        sortName: options.sortName,
        sortClass: "table-active",
        sortOrder: "asc",                   //排序方式
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1,                      //初始化加载第一页，默认第一页,并记录
        pageSize: 15,                     //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        queryParamsType: '',                //默认值为 'limit' ,在默认情况下 传给服务端的参数为：{"search":"","order":"asc","offset":0,"limit":10}
                                            // ''  {"searchText":"","sortOrder":"asc","pageSize":10,"pageNumber":1}

        search: false,                      //是否启用搜索框
        showColumns: false,                  //是否显示所有的列（选择显示的列）
        showRefresh: false,                  //是否显示刷新按钮
        showToggle: false,                     // 是否显示 切换试图（table/card）按钮
        cardView: false,                    // 设置为 true将显示card视图，适用于移动设备。否则为table试图，适用于pc
        detailView: false,                  //设置为 true 可以显示详细页面模式。
        minimumCountColumns: 2,             //最少允许的列数
        // clickToSelect: true,                //是否启用点击选中行
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        //得到查询的参数
        queryParams: function (params) {
            var queryParams = searchForm.serializeJsonObject();
            queryParams.pageSize = params.pageSize;
            queryParams.pageNumber = params.pageNumber;
            if (params.sortName) {
                queryParams.orderBy = params.sortName;
                queryParams.sortOrder = params.sortOrder;
            }

            return queryParams;
        },
        columns: options.columnModel,
        locale: "zh-CN",

        treeEnable: options.treeEnable,
        rootParentId: '0',
        idField: options.idField,
        treeShowField: options.treeShowField,
        parentIdField: options.parentIdField,

        onPostBody: function() {
            var columns = btGrid.bootstrapTable('getOptions').columns;
            if (columns && columns[0][1].visible) {
                btGrid.treegrid({
                    treeColumn: 0,
                    onChange: function() {
                        btGrid.bootstrapTable('resetWidth')
                    }
                });
            }
        },

        onLoadSuccess: function (data) {
            // 请求成功之后调用方法
            if (typeof options.ajaxSuccess == 'function'){
                options.ajaxSuccess(data);
            }

            // 绑定列表按钮事件
            if (typeof options.btnEventBind == 'function'){
                options.btnEventBind($('.btnList'));
            }

            // 如果有消息，则显示提示消息
            if (data && data.message){
                js.showMessage(data.message);
            }

            $('.btn').attr("disabled", false);
            js.closeLoading();
        },
        onLoadError: function (data) {
            if (typeof options.ajaxError == 'function'){
                options.ajaxError(data);
            }
            $('.btn').attr("disabled", false);
            if (data.responseText && data.responseText != ''){
                js.showErrorMessage(('操作失败，' + data.responseText + '！'));
            }
            js.closeLoading();
        },
        onDblClickRow: function (row, $element) {
            var id = row.ID;
        },
        // 按钮事件绑定
        btnEventBind: function(elements){
            elements.each(function(){
                var clickBinded = $(this).attr('data-click-binded');
                if (clickBinded == undefined){

                    // 防止重复绑定
                    $(this).attr('data-click-binded', true);
                    // 绑定按钮单击事件
                    $(this).click(function(){
                        var se = $(this);
                        var url = se.attr('href');
                        var title = se.data('title');
                        if (title == undefined){
                            title = se.attr('title');
                        }
                        var confirm = se.data('confirm');
                        var prompt = se.data('prompt');

                        if(confirm != undefined){
                            js.confirm(confirm, url, function(data){
                                js.showMessage(data.message);
                                if(data.result== 'true'){
                                    var confirmSuccess = se.data('confirmSuccess');
                                    if (confirmSuccess != undefined){
                                        try{
                                            eval(confirmSuccess);
                                        }catch(e){
                                            js.print('confirmSuccess error: ' + e);
                                        }
                                    }else{
                                        // 如果是树结构表格
                                        if (options.treeGrid){
                                            var row = getRowData(se.parents('.jqgrow').attr('id'));
                                            if (row && !isRoot(row.parentId)){
                                                refreshTree(1, row.parentId);
                                            }else{
                                                refreshTree();
                                            }
                                        }else{
                                            refresh();
                                        }
                                    }
                                }
                            }, "json");
                        } else if(prompt != undefined) {

                            js.prompt(prompt, url, function(data) {
                                js.showMessage(data.message);
                                if(data.result== 'true'){
                                    // 如果是树结构表格
                                    if (options.treeGrid){
                                        var row = getRowData(se.parents('.jqgrow').attr('id'));
                                        if (row && !isRoot(row.parentId)){
                                            refreshTree(1, row.parentId);
                                        }else{
                                            refreshTree();
                                        }
                                    }else{
                                        refresh();
                                    }
                                }
                            }, "json");

                        } else {
                            var type = se.data('type');
                            if(type != undefined && type == "layer") {

                                js.layer.open({
                                    type: 2,
                                    title: title,
                                    shade: false,
                                    shadeClose: false,
                                    maxmin: true, //开启最大化最小化按钮
                                    area: ['100%', '100%'],
                                    content: url,
                                    end: function() {
                                        refresh();
                                    }
                                });

                            } else if(type != undefined && type == "diagram") {

                                js.layer.open({
                                    type: 2,
                                    title: title,
                                    shade: 0.3,
                                    shadeClose: true,
                                    maxmin: true, //开启最大化最小化按钮
                                    area: ['80%', '80%'],
                                    content: url,
                                    end: function() {
                                        //refresh();
                                    }
                                });

                            } else if(type != undefined && type == "form") {

                                var formTitle = se.data('formtitle');
                                var formUrl = se.data('formurl');
                                var wh = se.data('wh');

                                js.form(formTitle, url, formUrl, function(data) {
                                    if(data.result == "true") {
                                        js.showMessage(data.message);
                                        refresh();
                                    } else {
                                        js.showErrorMessage(data.message);
                                    }

                                }, "json", wh);

                            } else {
                                var win;
                                if (window.tabpanel) {
                                    win = window;
                                }
                                if (parent.tabpanel) {
                                    win = window.parent;
                                }
                                if (parent.parent.tabpanel) {
                                    win = window.parent.parent;
                                }
                                if (top.tabpanel) {
                                    win = top;
                                }
                                win.addTabPage($(this), title, url, true, true);
                            }
                        }
                        return false;
                    });
                }
            });
            return self;
        },
    }, options);

// 绑定查询表单提交事件
    searchForm.submit(function(){
        refresh();
        return false;
    });

    // 绑定工具条上的按钮
    if (typeof options.btnEventBind == 'function'){
        // 绑定工具栏上的按钮
        options.btnEventBind($('.btnTool'));

        // 绑定查询按钮
        options.btnSearch.click(function() {

            var btnSearch = $(this);
            var height;
            if (searchForm.hasClass("d-none")) {
                searchForm.removeClass("d-none");
                btnSearch.html(btnSearch.html().replace("查询", "隐藏"));
                height = $(window).height()-searchForm.outerHeight()-55;
            } else {
                searchForm.addClass("d-none");
                btnSearch.html(btnSearch.html().replace("隐藏", "查询"));
                height = $(window).height()-52;
            }

            js.print("windowH:" + $(window).height() + "      height:" + height + "    searchHeight:" + searchForm.outerHeight() + "     card-headerH:" + $('.card-header').outerHeight());

            btGrid.bootstrapTable('resetView', {height:height});
            return false;
        });
    };

    // 初始化jqGrid
    btGrid.bootstrapTable(options);

    /**
     * 刷新 bootstrap 表格
     * Refresh the remote server data,
     * you can set {silent: true} to refresh the data silently,
     * and set {url: newUrl} to change the url.
     * To supply query params specific to this request, set {query: {foo: 'bar'}}
     */
    function refresh(parms) {
        if (typeof parms != "undefined") {
            btGrid.bootstrapTable('refresh', parms);
        } else {
            btGrid.bootstrapTable('refresh');
        }
    }
    /**
     * 获取选中行数据
     */
    function getSelections() {
        return btGrid.bootstrapTable('getSelections');
    }

    $(window).resize(function () {
        var height = $(window).height()-searchForm.outerHeight()-55;

        js.print("windowH:" + $(window).height() + "      height:" + height + "    searchHeight:" + searchForm.outerHeight() + "     card-headerH:" + $('.card-header').outerHeight());
        btGrid.bootstrapTable('resetView', {height:height});
    });
};

$.fn.BTGrid = function (option) {
    var $set = this.each(function () {
        var $this = $(this);
        var data = $this.data('btGrid');
        var options = typeof option === 'object' && option;
        if (!data) {
            data = new BSTable(options, $this);
            window[$this.attr('id')] = data;
            $this.data('btGrid', data);
        }
    });
    return $set;
};


$.fn.serializeJsonObject = function () {
    var json = {};
    var form = this.serializeArray();
    $.each(form, function () {
        if (json[this.name]) {
            if (!json[this.name].push) {
                json[this.name] = [json[this.name]];
            }
            json[this.name].push();
        } else {
            if (this.value) {
                json[this.name] = this.value || '';
            }
        }
    });
    return json;
}